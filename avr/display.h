#ifndef __display_h_included__
#define __display_h_included__
#include <avr/io.h>

void set_segment(uint8_t nr);
//void set_digit(uint16_t val, uint8_t dot);
void set_digit(uint8_t first, uint8_t second, uint8_t dot);

uint8_t display_mode; // 0 RAW data, 1 Clock data.
uint8_t display_one;
uint8_t display_two;
uint8_t display_dot;

#endif
