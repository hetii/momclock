#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <avr/wdt.h>
#include <avr/io.h>
#include "display.h"
#include "dimmer.h"
#include "clock.h"
#include "ir.h"

int8_t second_elapsed = 60;

void every_second(void){
  if (second_elapsed > 0) second_elapsed--;
  if (display_mode == 1){
    PORTD ^= (1<<1);
  } else {
    PORTD &= ~(1<<1);
  }
}

int main(void){
  DDRC |= (1<<2)|(1<<3)|(1<<5)|(1<<1)|(1<<0)|(1<<4);
  DDRD |= (1<<4)|(1<<0)|(1<<3)|(1<<5)|(1<<6);
  PORTC &= ~((1<<2)|(1<<3)|(1<<5)|(1<<1)|(1<<0)|(1<<4));
  PORTD &= ~((1<<4)|(1<<0)|(1<<3)|(1<<5)|(1<<6));
  DDRB |= (1<<1);

  PORTB &= ~(1<<1);
  DDRD &= ~(1<<2); 

  dimmer_init();
  ir_init(0);
  clock_init();

  display_one = 0;
  display_two = 0;

  display_mode = 1;
  wdt_enable(WDTO_1S);

  sei();

  int8_t data = -1;
  
  int8_t curr_seconds = -1;
  uint8_t set_time = 0;
  uint8_t pwm_val = 0;
  dimmer_level(pwm_val);
  // dimmer_level(display_one);
  for(;;){
    wdt_reset();
    data = ir_pool();

    switch(data){
      case 0:
        second_elapsed = 10;
        dimmer_level(0);
        pwm_val = 0;
        if (curr_seconds < 0 && seconds < 55){
          curr_seconds = seconds;
        } else if ( curr_seconds+5 == seconds && set_time == 0){
          set_time = 1;
          minutes = 33;
          //minutes = 0;
          hours = 33;
        }
        break;
      case 2:
        second_elapsed = 10;
        if (set_time == 1){
          if (++minutes > 59) minutes = 0;
        } else {
          
          if (pwm_val >= 220 ) {
            pwm_val = 255;
          } else {
            pwm_val += 5;
          }
          dimmer_level(pwm_val);
        }
        break;
      case 1:
        second_elapsed = 10;
        if (set_time == 1){
          if (++hours > 23) hours = 0;
        } else {        
          if (pwm_val <= 30){
            pwm_val = 0;
          } else {
            pwm_val -= 5;
          }
          dimmer_level(pwm_val);
        }
        break;
      case 3:
        second_elapsed = 10;
        dimmer_level(255);
        pwm_val = 255;
        break;
      default:
        if (second_elapsed == 0 && set_time == 1){
          dimmer_level(155);
          _delay_ms(500);
          dimmer_level(pwm_val);
          curr_seconds = -1;
          set_time = 0;
        }
        break;
    }
  }
  return 0;
}
