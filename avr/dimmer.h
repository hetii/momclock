#ifndef __dimmer_h_included__
#define __dimmer_h_included__

#define ACDET (1<<PD2)

#define TRIAC_OFF PORTB &= ~(1<<PB1)
#define TRIAC_ON  PORTB |= (1<<PB1)

#define nop asm volatile("nop")

// triac needs pulse width of 48 nops (clk = 16 MHz) to reliably trigger
// (empirically determined for an NTE 56003 triac)
#define triac_nop \
  nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; \
  nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; \
  nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; \
  nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;

// minimum phase delay, in timer ticks of 500 ns -- we need some buffer
// on either end of the intensity scale, near the zero crossings, to
// ensure that we don't try to trigger during the crossing or on
// the other side of it.
#define MIN_DELAY 1000

void dimmer_init(void);
void dimmer_level(char l);

#endif
