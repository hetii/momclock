#include <avr/interrupt.h>
#include "clock.h"

ISR(TIMER1_COMPA_vect){
  every_second();
  if(++seconds>=60){
    seconds=0;
    if(++minutes>=60){
      minutes=0;
      if(++hours>=24){
        hours=0;
      }
    }
  }
}

void clock_start(void){  
  // Configuration for clock timer1:
  TCCR1B = (1<<CS12)|(1<<WGM12); //P = 256, CTC mode.
  OCR1A = 46875;
  TIMSK |= (1 << OCIE1A);
}

void clock_stop(void){
  TCCR1B = 0x0;
  TIMSK &= ~(1 << OCIE1A);
  OCR1A = 0x00;
}

void clock_init(void){
  minutes=0;
  seconds=0;
  hours=0;

  clock_start();
}
