#ifndef __clock_h_included__
#define __clock_h_included__

volatile uint8_t minutes;
volatile uint8_t seconds;
volatile uint8_t hours;

extern inline void every_second(void);

void clock_start(void);
void clock_stop(void);
void clock_init(void);

#endif