#include "display.h"

/*
  K1: PB2  K2: PB1  K3: PC3  K4  PC4

        A PC2
      ---------
F PC1 |       | B PC3
      | G PD3 |          * P??
      ---------
E PD0 |       | C PC5
      |       |          *
      --------- 
        D PD4   * H P??
*/

static uint8_t digits[10] = {192, 249, 164, 176, 153, 146, 130, 248, 128, 144};
        
void set_segment(uint8_t nr){
  uint8_t val = digits[nr];
  uint8_t i = 0;
  while (i < 8) {
    if (val & 0x01) {
      switch (i) {
        case 0:PORTC |= (1<<2);break; // A
        case 1:PORTC |= (1<<3);break; // B
        case 2:PORTC |= (1<<5);break; // C
        case 3:PORTD |= (1<<4);break; // D
        case 4:PORTD |= (1<<0);break; // E
        case 5:PORTC |= (1<<1);break; // F
        case 6:PORTD |= (1<<3);break; // G
        //case 7:PORTB |= (1<<4);break; // H
        //case 8:PORTB |= (1<<4);break; // Dot
      }
    } else {
      switch (i) {
        case 0:PORTC &= ~(1<<2);break; // A
        case 1:PORTC &= ~(1<<3);break; // B
        case 2:PORTC &= ~(1<<5);break; // C
        case 3:PORTD &= ~(1<<4);break; // D
        case 4:PORTD &= ~(1<<0);break; // E
        case 5:PORTC &= ~(1<<1);break; // F
        case 6:PORTD &= ~(1<<3);break; // G
        //case 7:PORTB &= ~(1<<4);break; // H
        //case 8:PORTB &= ~(1<<4);break; // Dot
      }
    }
    i++;
    val = val >> 1;
  }
}

void set_digit(uint8_t first, uint8_t second, uint8_t dot){
  static uint8_t segment=1;

  //if (dot == segment) PORTB &= ~(1<<4); else PORTB |= (1<<4);

  switch (segment) {
    case 8:    
      if ((first / 10) == 0) break;
      set_segment(first / 10);
      PORTC &= ~((1<<PC0)|(1<<PC4));
      PORTD |= (1<<PD5);
      PORTD &= ~(1<<PD6);
    break;
    case 4:
      set_segment(first % 10);
      PORTC &= ~((1<<PC0)|(1<<PC4));
      PORTD |= (1<<PD6);
      PORTD &= ~(1<<PD5);
    break;
    case 2:
      set_segment(second / 10);
      PORTC |= (1<<PC4);
      PORTC &= ~(1<<PC0);
      PORTD &= ~((1<<PD5)|(1<<PD6));
    break;
    case 1:
      set_segment(second % 10);
      PORTC |= (1<<PC0);
      PORTC &= ~(1<<PC4);
      PORTD &= ~((1<<PD5)|(1<<PD6));
    break;
  }
  segment = (segment<<1)>8?1:segment<<1;
}