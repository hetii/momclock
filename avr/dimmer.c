#include <avr/interrupt.h>
#include <util/delay.h>
#include "display.h"
#include "dimmer.h"
#include "clock.h"

static uint8_t lvl = 43;
static uint16_t lvl_counts[256];
static volatile uint16_t zc = 0;
static volatile uint8_t ddd = 0;

static inline void pulse_triac(){
  if (lvl != 0){
    TRIAC_ON;
    triac_nop;
    TRIAC_OFF;    
  } else {
    TRIAC_OFF;
  }
}

ISR(INT0_vect) {
  zc = 0;
}

ISR(TIMER0_OVF_vect){
  if (zc++ >= lvl_counts[lvl]/256){
    pulse_triac();
  }

  if (ddd++ >= 25){
    if (display_mode == 1){
      set_digit(hours, minutes, 0);
    } else {
      set_digit(display_one, display_two, display_dot);
    }
    ddd = 0;
  }
}

void dimmer_level(char l){
  lvl = l;
}

void dimmer_init(void){
    //DDRD = (DDRD & 0xf3) | 0x08; // PD3 out, PD2 in
  TCCR1A = 0x00;
  TCCR1B = (1<<CS11); // timer: Clock / 8 == 2 MHz (mains halfcycle ~ 16667 counts)  - Preskaler 8

  // initialize by timing one half-cycle
  uint16_t halfcount;

  while(!(PIND & ACDET)) nop; // wait until we're in a negative half-cycle
  while((PIND & ACDET)) nop; // now wait for the beginning of pos half-cycle
  TCNT1 = 0;
  while(!(PIND & ACDET)) nop; // wait for the negative half-cycle again
  halfcount = TCNT1;

  // now calculate the timing table
  int i;
  int step = halfcount >> 8;
  int value = 0;
  for (i = 255; i >= 0; i--, value += step){
    lvl_counts[i] = value;
    // enforce minimum and maximum phase delay limits
    if (lvl_counts[i] < MIN_DELAY)
      lvl_counts[i] = MIN_DELAY;
    if (lvl_counts[i] > (halfcount - MIN_DELAY))
      lvl_counts[i] = (halfcount - MIN_DELAY);
  }
  // now set up the AC detector interrupt (INT0)
  MCUCR = (MCUCR & 0xfc) | 0x01; // ISC01:ISC00 = 01b: int on rise or fall
  GICR = (GICR & 0x3f) | 0x40; // enable INT0

  // enable the timer compare interrupt
  TCCR0 |= (1 << CS01);  // P = 8
  TIMSK |= (1 << TOIE0); // Enable below interrupt TIMER0_OVF_vect 
}